#!/usr/bin/env python

"""

Adapted from CondCore/Utilities/scripts/conditionUploadTest.py for use with new conditions upload service.

"""

import cx_Oracle
import subprocess
import json
import os
import shutil
import datetime
import sys
import CondCore.Utilities.CondDBFW as CondDBFW
import CondCore.Utilities.CondDBFW.uploads as CondDBFW_uploads
import netrc
import sqlalchemy

# Requirement 1: a conddb key for the authentication with valid permission on writing on prep CMS_CONDITIONS account 
#                this could be dropped introducing a specific entry in the .netrc 
# Requirement 2: an entry "ConditionUploader" in the .netrc for the authentication

sqlite_db = "sqlite:////data/services/db_upload_test/app/test_hlt.sqlite"
oracle_prep_db = "oracle://cms_orcoff_prep/CMS_CONDITIONS"

netrc_file = None
server = None

class DB:
    def __init__(self, serviceName, schemaName ):
        self.serviceName = serviceName
        self.schemaName = schemaName
        self.connStr = None

    def connect( self ):
        # use netrc to read the netrc file given
        """
        NOTE: This assumes the account is the schema owner.  If this changes, so must this code.
        """
        netrc_authenticators = netrc.netrc(netrc_file).authenticators("%s/%s/write" % (self.serviceName, self.schemaName))
        password = netrc_authenticators[2]
        self.connStr =  'oracle://%s:%s@%s' %(self.schemaName,password,self.serviceName)

    def setSynchronizationType( self, tag, synchType ):
        engine = sqlalchemy.create_engine(self.connStr)
        db = engine.connect()
        db.execute('UPDATE TAG SET SYNCHRONIZATION =:SYNCH WHERE NAME =:NAME',(synchType,tag,))
        db.close()

    def getLastInsertedSince( self, tag, snapshot ):
        engine = sqlalchemy.create_engine(self.connStr)
        db = engine.connect()
        result = db.execute('SELECT SINCE, INSERTION_TIME FROM IOV WHERE TAG_NAME =:TAG_NAME AND INSERTION_TIME >:TIME ORDER BY INSERTION_TIME DESC',(tag,snapshot))
        row = result.fetchone()
        db.close()
        return row

    def removeTag( self, tag ):
        engine = sqlalchemy.create_engine(self.connStr)
        db = engine.connect()
        db.execute('DELETE FROM IOV WHERE TAG_NAME =:TAG_NAME',(tag,))
        db.execute('DELETE FROM TAG WHERE NAME=:NAME',(tag,))
        db.close()

def makeBaseFile( inputTag, startingSince ):
    cwd = os.getcwd()
    baseFile = '%s_%s.db' %(inputTag,startingSince)
    baseFilePath = os.path.join(cwd,baseFile)
    if os.path.exists( baseFile ):
        os.remove( baseFile )
    command = "conddb_import -c sqlite_file:%s -f oracle://cms_orcon_adg/CMS_CONDITIONS -i %s -t %s -b %s" %(baseFile,inputTag,inputTag,startingSince)
    pipe = subprocess.Popen( command, shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
    out = pipe.communicate()[0]
    if not os.path.exists( baseFile ):
        msg = 'ERROR: base file has not been created: %s' %out
        raise Exception( msg )
    return baseFile

def makeMetadataSource(databaseFileName, inputTag, destTag, since, description):
    # use netrc to get username and password
    try:
        netrc_authenticators = netrc.netrc().authenticators("ConditionUploader")
        if netrc_authenticators == None:
            print("netrc file must contain the key 'ConditionUploader'.")
            exit()
    except:
        print("Something went wrong when opening ~/.netrc to obtain your credentials.")
        exit()

    username = netrc_authenticators[0]
    password = netrc_authenticators[2]

    dictionary = {"sourceDB" : databaseFileName, "inputTag" : inputTag, "destinationTags" : {destTag:{}},\
                 "destinationDatabase" : oracle_prep_db, "since" : since, "fcsr_filter" : None,\
                 "userText" : None, "username" : username, "password" : password}

    json_dictionary = CondDBFW.data_sources.json_data_node.make(dictionary)

    return json_dictionary

def doUpload(metadataSource):
    uploader = CondDBFW_uploads.uploader(metadata_source=metadataSource, debug=True, verbose=False, testing=True, server=server)
    success = uploader.upload()
    if not(success):
        print("Exception occurred.")
        return False
    else:
        return True

class UploadTest:
    def __init__(self, db):
        self.db = db
        self.errors = 0
        self.logFileName = 'conditionUploadTest.log'

    def log( self, msg ):
        print msg
        with open(self.logFileName,'a') as logFile:
            logFile.write(msg)
            logFile.write('\n')

    def upload( self, inputTag, baseFile, destTag, synchro, destSince, success, expectedAction ):
        insertedSince = None
        destFile = '%s.db' %destTag
        shutil.copyfile( baseFile, destFile )
        self.log( '# ---------------------------------------------------------------------------')
        self.log( '# Testing tag %s with synch=%s, destSince=%s - expecting ret=%s action=%s' %(destTag,synchro,destSince,success,expectedAction))
    
        descr = 'Testing conditionsUpload with synch:%s - expected action: %s' %(synchro,expectedAction)
        metadataSource = makeMetadataSource( destFile, inputTag, destTag, destSince, descr )

        beforeUpload = datetime.datetime.now()
        print("Upload starting at %s" % str(beforeUpload))
        ret = doUpload( metadataSource )
        if ret != success:
            self.log( 'ERROR: the return value for the upload of tag %s with sychro %s was %s, while the expected result is %s' %(destTag,synchro,ret,success))
            self.errors += 1
        else:
            row = self.db.getLastInsertedSince( destTag, beforeUpload )
            if ret == True:
                if expectedAction == 'CREATE' or expectedAction == 'INSERT' or expectedAction == 'APPEND':
                    if destSince != row[0]:
                        self.log( 'ERROR: the since inserted is %s, expected value is %s - expected action: %s' %(row[0],destSince,expectedAction))
                        self.errors += 1
                    else:
                        self.log( '# OK: Found expected value for last since inserted: %s timestamp: %s' %(row[0],row[1]))
                        insertedSince = row[0]
                elif expectedAction == 'SYNCHRONIZE':
                    if destSince == row[0]:
                        self.log( 'ERROR: the since inserted %s has not been synchronized with the FCSR - expected action: %s' %(row[0],expectedAction))
                        self.errors += 1
                    else:
                        self.log( '# OK: Found synchronized value for the last since inserted: %s timestamp: %s' %(row[0],row[1]))
                        insertedSince = row[0]
                else:
                    self.log( 'ERROR: found an appended since %s - expected action: %s' %(row[0],expectedAction))
                    self.errors += 1
            else:
                if not row is None:
                    self.log( 'ERROR: found new insered since: %s timestamp: %s' %(row[0],row[1]))
                    self.errors += 1
                if expectedAction != 'FAIL':
                    self.log( 'ERROR: Upload failed. Expected value: %s' %(destSince))
                    self.errors += 1
                else:
                    self.log( '# OK: Upload failed as expected.')
        os.remove( destFile )
        return insertedSince


def main():
    print 'Testing...'
    print("Using server instance '%s'." % server)
    serviceName = 'cms_orcoff_prep'
    schemaName = 'CMS_CONDITIONS'
    db = DB(serviceName,schemaName)
    db.connect()
    inputTag = 'runinfo_31X_mc'
    bfile0 = makeBaseFile( inputTag,1)
    bfile1 = makeBaseFile( inputTag,100)

    test = UploadTest( db )

    # test with synch=any
    tag = 'test_CondUpload_any'
    test.upload( inputTag, bfile0, tag, 'any', 1, True, 'CREATE' )  

    test.upload( inputTag, bfile1, tag, 'any', 1, False, 'FAIL' )  

    test.upload( inputTag, bfile0, tag, 'any', 200, True, 'APPEND' )  
    test.upload( inputTag, bfile0, tag, 'any', 100, True, 'INSERT')  
    test.upload( inputTag, bfile0, tag, 'any', 200, True, 'INSERT')  

    db.removeTag( tag )

    # test with synch=validation
    tag = 'test_CondUpload_validation'
    test.upload( inputTag, bfile0, tag, 'validation', 1, True, 'CREATE')  
    db.setSynchronizationType( tag, 'validation' ) 
    test.upload( inputTag, bfile0, tag, 'validation', 1, True, 'INSERT')  
    test.upload( inputTag, bfile0, tag, 'validation', 200, True, 'APPEND')  
    test.upload( inputTag, bfile0, tag, 'validation', 100, True, 'INSERT')  
    db.removeTag( tag )
    # test with synch=mc
    tag = 'test_CondUpload_mc'
    test.upload( inputTag, bfile1, tag, 'mc', 1, False, 'FAIL')  
    test.upload( inputTag, bfile0, tag, 'mc', 1, True, 'CREATE')  
    db.setSynchronizationType( tag, 'mc' ) 
    test.upload( inputTag, bfile0, tag, 'mc', 1, False, 'FAIL')  
    test.upload( inputTag, bfile0, tag, 'mc', 200, False, 'FAIL') 
    db.removeTag( tag )
    # test with synch=hlt
    tag = 'test_CondUpload_hlt'
    test.upload( inputTag, bfile0, tag, 'hlt', 1, True, 'CREATE')  
    db.setSynchronizationType( tag, 'hlt' ) 
    test.upload( inputTag, bfile0, tag, 'hlt', 200, True, 'SYNCHRONIZE')  
    fcsr = test.upload( inputTag, bfile0, tag, 'hlt', 100, True, 'SYNCHRONIZE')  
    if not fcsr is None:
        since = fcsr + 200
        test.upload( inputTag, bfile0, tag, 'hlt', since, True, 'APPEND')  
        since = fcsr + 100
        test.upload( inputTag, bfile0, tag, 'hlt', since, True, 'INSERT')  
    db.removeTag( tag )
    # test with synch=express
    tag = 'test_CondUpload_express'
    test.upload( inputTag, bfile0, tag, 'express', 1, True, 'CREATE')  
    db.setSynchronizationType( tag, 'express' ) 
    test.upload( inputTag, bfile0, tag, 'express', 200, True, 'SYNCHRONIZE')  
    fcsr = test.upload( inputTag, bfile0, tag, 'express', 100, True, 'SYNCHRONIZE')  
    if not fcsr is None:
        since = fcsr + 200
        test.upload( inputTag, bfile0, tag, 'express', since, True, 'APPEND')  
        since = fcsr + 100
        test.upload( inputTag, bfile0, tag, 'express', since, True, 'INSERT')  
    db.removeTag( tag )
    # test with synch=prompt
    tag = 'test_CondUpload_prompt'
    test.upload( inputTag, bfile0, tag, 'prompt', 1, True, 'CREATE')  
    db.setSynchronizationType( tag, 'prompt' ) 
    test.upload( inputTag, bfile0, tag, 'prompt', 200, True, 'SYNCHRONIZE')  
    fcsr = test.upload( inputTag, bfile0, tag, 'prompt', 100, True, 'SYNCHRONIZE')  
    if not fcsr is None:
        since = fcsr + 200
        test.upload( inputTag, bfile0, tag, 'prompt', since, True, 'APPEND')  
        since = fcsr + 100
        test.upload( inputTag, bfile0, tag, 'prompt', since, True, 'INSERT')  
    db.removeTag( tag )
    # test with synch=pcl
    tag = 'test_CondUpload_pcl'
    test.upload( inputTag, bfile0, tag, 'pcl', 1, True, 'CREATE')  
    db.setSynchronizationType( tag, 'pcl' ) 
    test.upload( inputTag, bfile0, tag, 'pcl', 200, False, 'FAIL')  
    if not fcsr is None:
        since = fcsr + 200
        test.upload( inputTag, bfile0, tag, 'pcl', since, True, 'APPEND')  
        since = fcsr + 100
        test.upload( inputTag, bfile0, tag, 'pcl', since, True, 'INSERT')  
    db.removeTag( tag )
    # test with synch=offline
    tag = 'test_CondUpload_offline'
    test.upload( inputTag, bfile0, tag, 'offline', 1, True, 'CREATE')  
    db.setSynchronizationType( tag, 'offline' ) 
    test.upload( inputTag, bfile0, tag, 'offline', 1000, True, 'APPEND')
    test.upload( inputTag, bfile0, tag, 'offline', 500, False, 'FAIL' ) 
    test.upload( inputTag, bfile0, tag, 'offline', 1000, False, 'FAIL' ) 
    test.upload( inputTag, bfile0, tag, 'offline', 2000, True, 'APPEND' ) 
    db.removeTag( tag )
    # test with synch=runmc
    tag = 'test_CondUpload_runmc'
    test.upload( inputTag, bfile0, tag, 'runmc', 1, True, 'CREATE')  
    db.setSynchronizationType( tag, 'runmc' ) 
    test.upload( inputTag, bfile0, tag, 'runmc', 1000, True, 'APPEND')
    test.upload( inputTag, bfile0, tag, 'runmc', 500, False, 'FAIL' ) 
    test.upload( inputTag, bfile0, tag, 'runmc', 1000, False, 'FAIL' ) 
    test.upload( inputTag, bfile0, tag, 'runmc', 2000, True, 'APPEND' ) 
    db.removeTag( tag )
    os.remove( bfile0 )
    os.remove( bfile1 )

    print 'Done. Errors: %s' %test.errors
    
if __name__ == '__main__':
    try:
        server_alias_to_url = {
            "prep" : "https://cms-conddb-dev.cern.ch/cmsDbCondUpload/",
            "prod" : "https://cms-conddb.cern.ch/cmsDbCondUpload/",
            None : "https://cms-conddb.cern.ch/cmsDbCondUpload/"
        }
        netrc_file = sys.argv[1]
        server = server_alias_to_url[sys.argv[2]]
    except Exception:
        print("Incorrect arguments given - should be: netrc_file server_alias\nnetrc_file : file name, server : prep or prod")
        exit()
    main()
